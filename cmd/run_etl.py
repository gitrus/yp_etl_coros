import click as click

from src.config import ETLSettings
from src.main import run_movie


@click.command()
def main():
    settings = ETLSettings()

    run_movie(settings)
