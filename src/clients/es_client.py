import typing as t
from contextlib import contextmanager
from itertools import chain

import backoff
from elasticsearch import Elasticsearch
from elasticsearch.helpers import streaming_bulk
from more_itertools import chunked
from pydantic import HttpUrl


class ESBatchClient:
    def __init__(self, es_http: HttpUrl, batch_size: int = 100):
        self._buffer = []
        self._batch_size = batch_size
        self._es_client = Elasticsearch(es_http)

        self._cur_index_name = None
        self._backoff_max_tries = 3

    @contextmanager
    def connect(self, index_name: str, *, batch_size: t.Optional[int] = None):
        try:
            self._cur_index_name = index_name
            self._batch_size = batch_size or self._batch_size
            yield self
        finally:
            self.flush()
            self._cur_index_name = None

    def load_batch(self, data: list[dict]):
        if len(self._buffer) + len(data) < self._batch_size:
            self._buffer.extend(data)
            return

        chained_data = chain(self._buffer, data)
        for data_chunk in chunked(chained_data, self._batch_size):
            self._flush(data_chunk)

        self._buffer.clear()

    def flush(self):
        self._flush(self._buffer)
        self._buffer.clear()

    def _flush(self, data_buffer: list[dict]):
        if not len(data_buffer):
            return

        backoff_failed_actions = backoff.on_predicate(
            wait_gen=backoff.expo,
            predicate=lambda ret: bool(len(ret)),
            max_tries=self._backoff_max_tries,
        )

        load_data = backoff_failed_actions(self._load_data)

        load_data(data_buffer)

    def _load_data(self, actions: list[dict]):
        failed_actions = []
        for ok, action in streaming_bulk(
            client=self._es_client,
            index=self._cur_index_name,
            actions=actions,
        ):
            if not ok:
                failed_actions.append(action)

        return failed_actions
