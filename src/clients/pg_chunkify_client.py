import logging
import typing as t

import psycopg2
from psycopg2._psycopg import connection as Connection
from psycopg2.extras import RealDictCursor
from pydantic import BaseModel, PostgresDsn


class PGChunkifyClient(BaseModel):
    """realization sql manager for postgresql"""

    dsn: PostgresDsn

    _conn: t.Optional[Connection] = None
    _logger = logging.getLogger(__name__)

    def _init_connection(self):
        self._conn = psycopg2.connect(dsn=self.dsn)

    def _close_connection(self):
        """Method makes closing connecion to database"""
        self._conn.close()

    def extract_by_batch(
        self,
        query: str,
        parameters: t.Union[tuple, dict, None] = None,
        batch_size: int = 100,
    ) -> t.Iterable[list[dict]]:
        if not self._conn:
            self._init_connection()

        with self._conn.cursor(cursor_factory=RealDictCursor) as cur:
            cur.execute(query, parameters)
            self._logger.info("some")
            while records := cur.fetchmany(size=batch_size):
                yield records
