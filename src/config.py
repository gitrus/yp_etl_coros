from pydantic import BaseSettings, PostgresDsn, RedisDsn, HttpUrl


class ETLSettings(BaseSettings):
    POSTGRES_DSN: PostgresDsn
    REDIS_DSN: RedisDsn
    ES_DSN: HttpUrl
    BATCH_SIZE: int = 2000

    LOGGING_CONFIG_FILE = "logging.ini"
    MAX_BACKOFF_DECAY = 60
    ITERATION_INTERVAL = 0.1
    CHECK_DB_INTERVAL = 3
    BACKOFF_FACTOR = 0.1
    BACKOFF_MAX_WAIT = 10
    ES_INDEX = "movies"

    class Config:
        env_file = ".env"
        env_file_encoding = "utf-8"
        fields = {
            "POSTGRES_DSN": {
                "env": "POSTGRES_DSN",
            },
            "REDIS_DSN": {
                "env": "REDIS_DSN",
            },
            "BATCH_SIZE": {"env": "BATCH_SIZE"},
        }
