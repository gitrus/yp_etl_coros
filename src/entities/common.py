from datetime import datetime
from typing import Type

from pydantic import BaseModel


class ETLEntity(BaseModel):
    entity_name: str
    index_name: str

    entity_type: Type[BaseModel]


class EntityState(BaseModel):
    entity_name: str

    cursor_column_name: str
    cursor_value: str

    last_modified_at: datetime
