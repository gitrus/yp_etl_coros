from datetime import datetime
from enum import Enum

from pydantic import BaseModel

from .common import ETLEntity


class Person(BaseModel):
    id: str
    name: str


class Movie(BaseModel):
    id: str
    title: str
    description: str
    rating: float
    type: str
    genre: str
    actors_names: list[str] = []
    actors: list[Person] = []
    director: str = ""
    writers_names: list[str] = []
    writers: list[Person] = []
    modified: datetime


class ETLEntityEnum(Enum):
    MOVIE = ETLEntity(entity_name="movie", index_name="movies", entity_type=Movie)
    GENRE = ETLEntity(entity_name="person", index_name="persons", entity_type=Person)
