import typing as t
from datetime import datetime
from datetime import timedelta

from src.clients.pg_chunkify_client import PGChunkifyClient
from src.entities.common import EntityState
from src.entities.movie_db import ETLEntityEnum

MOVIES_SQL = """SELECT
   fw.id,
   fw.title,
   fw.description,
   fw.rating,
   fw.type,
   fw.created,
   fw.modified,
   COALESCE (
       json_agg(
           DISTINCT jsonb_build_object(
               'person_role', pfw.role,
               'person_id', p.id,
               'person_name', p.full_name
           )
       ) FILTER (WHERE p.id is not null),
       '[]'
   ) as persons,
   array_agg(DISTINCT g.name) as genres
FROM content.film_work fw
LEFT JOIN content.person_film_work pfw ON pfw.film_work_id = fw.id
LEFT JOIN content.person p ON p.id = pfw.person_id
LEFT JOIN content.genre_film_work gfw ON gfw.film_work_id = fw.id
LEFT JOIN content.genre g ON g.id = gfw.genre_id
WHERE fw.modified >= '{last_modified}'
GROUP BY fw.id
ORDER BY fw.modified;"""

entity_to_sql = {ETLEntityEnum.MOVIE: MOVIES_SQL}


def postgres_extractor(
    client: PGChunkifyClient, entity: ETLEntityEnum, entity_state: EntityState
) -> t.Iterable[dict]:
    last_modified = datetime.fromisoformat(entity_state.cursor_value) - timedelta(milliseconds=1)
    for batch in client.extract_by_batch(entity_to_sql[entity], {"modified": last_modified}):
        for rec in batch:
            yield rec
