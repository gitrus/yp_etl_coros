import typing as t

from src.clients.es_client import ESBatchClient
from src.entities.movie_db import ETLEntityEnum
from src.utils import etl_coro


@etl_coro
def load_to_es(
    es_client: ESBatchClient,
    etl_entity: ETLEntityEnum,
) -> t.Generator[None, dict, None]:
    with es_client.connect(etl_entity.value.index_name) as es_conn:
        while data := (yield):
            es_conn.load_batch([data])
