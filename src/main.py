import typing as t
from functools import partial

from redis.client import Redis

from src.state.repository import IStateRepo
from src.state.repository import StateRepo
from .clients.es_client import ESBatchClient
from .clients.pg_chunkify_client import PGChunkifyClient

from .config import ETLSettings
from .entities.common import EntityState
from .entities.movie_db import ETLEntityEnum
from .extract.extractors import postgres_extractor
from .load.loader import load_to_es
from .transform.transforms import type_cast_transform_movie


def etl(
    etl_entity: ETLEntityEnum,
    state: EntityState,
    extractor: t.Generator,
    transformer: t.Genertor,
    loader: t.Generator,
):
    extractor = extractor(etl_entity, state)
    transformer = transformer()
    loader = loader(etl_entity)

    for rec in extractor:
        transformed_datum = transformer.send(rec)
        loader.send(transformed_datum)


def run_movie(settings: ETLSettings):
    state_repo: IStateRepo = StateRepo(_redis_client=Redis.from_url(url=settings.REDIS_DSN))

    state = state_repo.get_state()

    client = PGChunkifyClient(dsn=settings.POSTGRES_DSN)
    pg_extractor = partial(postgres_extractor, client)

    es_client = ESBatchClient(settings.ES_DSN)
    es_loader = partial(load_to_es, es_client)

    etl(
        etl_entity=ETLEntityEnum.MOVIE,
        state=state.entities_states[0],
        extractor=pg_extractor,
        transformer=type_cast_transform_movie,
        loader=es_loader,
    )
