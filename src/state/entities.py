from datetime import datetime

from pydantic import BaseModel

from src.entities.common import EntityState


class State(BaseModel):
    """
    It is class for saving state of ETL
    """

    last_start_at: datetime
    last_end_at: datetime

    entities_states: list[EntityState]
