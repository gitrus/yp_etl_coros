from typing import Protocol

from pydantic import BaseModel
from redis import Redis

from .entities import State


class IStateRepo(Protocol):
    def get_state(self) -> State:
        ...

    def set_state(self, state: State) -> None:
        ...


class StateRepo(BaseModel):
    _redis_client: Redis
    _state_key: str = "etl_state"

    def get_state(self) -> State:
        raw_state = self._redis_client.hgetall(self._state_key)
        state = State(**raw_state)
        return state

    def set_state(self, state: State) -> None:
        raw_state = state.dict()
        self._redis_client.hset(self._state_key, mapping=raw_state)
