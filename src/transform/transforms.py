import time
import typing as t
import logging
from enum import Enum

from src.utils import etl_coro

from src.entities.movie_db import Movie, Person


class Role(str, Enum):
    ACTOR = "actor"
    DIRECTOR = "director"
    WRITER = "writer"


_logger = logging.getLogger(__name__)


@etl_coro
def type_cast_transform_movie() -> t.Generator[Movie, dict, None]:
    """
    function-coroutine: transforms raw result sql query to structured data for load to ES
    """
    transformed_movie = None
    while movie := (yield transformed_movie):
        transformed_movie = Movie(**movie)

        person_id = movie["person_id"]
        person_name = movie["person_name"]
        person = Person(id=person_id, name=person_name)

        role = Role[movie["role"]]
        if role is Role.ACTOR:
            transformed_movie.actors.append(person)
            transformed_movie.actors_names.append(person_name)
        elif role is Role.DIRECTOR:
            transformed_movie.director = person_name
        elif role is Role.WRITER:
            transformed_movie.writers.append(person)
            transformed_movie.writers_names.append(person_name)
        else:
            _logger.warning(f"Missed role {movie}")
