from functools import wraps


def etl_coro(f):
    """util function for create etl-coro and start it"""

    @wraps(f)
    def wrapper(*args, **kwargs):
        f_coro = f(*args, **kwargs)
        f_coro.next()

        return f_coro

    return wrapper
